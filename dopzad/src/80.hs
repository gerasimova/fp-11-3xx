check :: Integer -> Integer -> Integer
check x result | x == 100 = myShow result
		       | mySqrt x = check (x + 1) result
               | otherwise = drob x 10 0 result
		
mySqrt :: Integer -> Bool		
mySqrt x | sqrt(fromIntegral (x)) * sqrt(fromIntegral (x)) == fromIntegral (x) = True
		 | otherwise = False
		
myShow :: Integer -> Integer
myShow x = x		
		
drob :: Integer -> Integer -> Integer -> Integer -> Integer
drob x ten count result | count == 100 = check (x + 1) result
                        | otherwise = drob  x (ten * 10) (count + 1) (add x ten result)

add :: Integer -> Integer -> Integer -> Integer
add x ten result = result + (truncate (mult x ten) `mod` 10)

mult :: Integer -> Integer -> Double
mult x ten = sqrt (fromIntegral (x)) * fromIntegral (ten)